module Main where
import Graphics.Gloss
import Graphics.Gloss.Interface.IO.Display
import Graphics.Gloss.Export.PNG
import Graphics.UI.GLUT.Begin
import Dibujo
import Interp
import Data.List
import System.Directory
import qualified Basico.Escher as E
import qualified Basico.FromFile as F

{-
librerias para que eventualmente se pueda exportar a PNG:
import Graphics.Gloss.Export.PNG
import ToFile
-}

{- En este archivo están las llamadas principales del programa. 
Este archivo llama a los otros y administra lo que es el tomar 
la imagen semilla, y el guardado en un archivo aparte -}

data Conf a = Conf {
    basic :: Output a
  , fig  :: Dibujo a
  , width :: Float
  , height :: Float
  }

ej x y = Conf {
                basic = E.interpBas
              , fig = E.ejemplo
              , width = x
              , height = y
              }

-- Dada una computación que construye una configuración, mostramos por
-- pantalla la figura de la misma de acuerdo a la interpretación para
-- las figuras básicas. Permitimos una computación para poder leer
-- archivos, tomar argumentos, etc.
initial :: IO (Conf a) -> IO ()
initial cf = cf >>= \cfg ->
                  let x  = width cfg
                      y  = height cfg
                      pic = interp (basic cfg) (fig cfg)
                   in exportPictureToPNG (1500, 1488) white "dibujo.png" $ pic (-x / 2,-y / 2) (x,0) (0,y)
                  --where
                  --  transp = makeColorI 0 0 0 1
                  --in display win white . withGrid $ trian1 (0, 0) (x, 0) (0, y)
  --where withGrid p = pictures [p, color grey $ grid 25 (0,0) 100 10]
  --      grey = makeColorI 120 120 120 120

win = InWindow "Nice Window" (500, 500) (0, 0)
--main antes de hacerlo que tome archivos como seed era el siguiente:
--main = initial $ return (ej 100 100)

--ahora el main es este:
{- comentarios generales sobre las imágenes seed
como verás, las seeds tienen que estar en ./img
tienen que ser bmp, esto nosotres lo hacemos creando pngs(para que tenga
transparencias). Luego usamos el comando convert para pasarlo a bmp.
La tupla antes del else en la linea 58 son las dimensiones de la imagen seed...
-}
main = do files <- getDirectoryContents "./img"
          let filtered = filter (isSuffixOf ".bmp") files
          putStrLn "DIBUJOS DISPONIBLES:"
          mapM_ putStrLn filtered
          putStr "-> Elegir dibujo: "
          dibujo <- getLine
          if elem dibujo filtered then initial $ imgFromFile ("img/" ++ dibujo) (472, 488) else main
          --imgFromFile ("img/" ++ dibujo) (dim1, dim2) vendría a ser la picture que querés guardar en un archivo

        --intento de hacer que tome dos imágenes como seed (este sí anda!!)
        --las superpone solamente (por ahora)
          {- putStrLn "¿CON CUANTOS ARCHIVOS QUERES DIBUJAR? (1 O 2)"
          option <- getLine
          if option == "1" then do
              putStrLn "DIBUJOS DISPONIBLES:"
              mapM_ putStrLn filtered
              putStr "-> Elegir dibujo: "
              dibujo <- getLine
              if elem dibujo filtered then initial $ imgFromFile ("img/" ++ dibujo) (300, 300) else main
          else do
               putStrLn "DIBUJOS DISPONIBLES:"
               mapM_ putStrLn filtered
               putStr "-> Elegir dibujo1: "
               dibujo1 <- getLine
               putStr "-> Elegir dibujo 2: "
               dibujo2 <- getLine
               if (elem dibujo1 filtered) && (elem dibujo2 filtered) then 
                    initial $ imgFromFile' ("img/" ++ dibujo1) ("img/" ++ dibujo2) (10000, 10000) else main
-}

imgFromFile :: String -> Vector -> IO (Conf F.File)
imgFromFile s (x, y) = do
                    img <- loadBMP s
                    return $ Conf {
                                    basic  = F.interpBas img (x, y)
                                  -- interpBas img (x, y) devuelve una función que al interpretar una básica
                                  -- devuelve img si es la del archivo o otra si no lo es.
                                  , fig    = F.ejemplo
                                  , width  = x
                                  , height = y
                                  }
